﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using COKIAPP.Droid.Services;
using COKIAPP.Services;
using Firebase.Auth;
using Xamarin.Auth;
using Xamarin.Forms;

[assembly: Dependency(typeof(FirebaseAuthService))]
namespace COKIAPP.Droid.Services
{
    public class FirebaseAuthService : IFirebaseAuthService
    {
        public const string KEY_AUTH_GOOGLE = "AUTH_GOOGLE";
        public const string KEY_AUTH_FACEBOOK = "AUTH_FACEBOOK";

        #region Configuracion OAuth 
        public const string GOOGLE_CLIENT_ID = "701581888733-8saqpjb86gnkr7el16c6kkf7j2q3ji7n.apps.googleusercontent.com";
        public static int GOOGLE_REQ_AUTH = 9999;

        private const string FACEBOOK_APP_ID = "1790813007670674";
        private const string FACEBOOK_AUTH_SCOPE = "email";
        private const string FACEBOOK_AUTH_URI = "https://www.facebook.com/dialog/oauth/";
        private const string FACEBOOK_REDIRECT_URI = "https://xamarintestproyect.firebaseapp.com/__/auth/handler";
        #endregion
        #region Campos
        public static OAuth2Authenticator XAuth { get; private set; }
        #endregion

        #region Key de Autenticacion
        public string GoogleAuthKey => KEY_AUTH_GOOGLE;

        public string FacebookAuthKey => KEY_AUTH_FACEBOOK;
        #endregion

        #region Obtener Usuario de Firebase
        public IFirebaseUser GetUser()
        {
            return new FirebaseUserWrapper(Firebase.Auth.FirebaseAuth.GetInstance(MainActivity.App).CurrentUser);
        }
        public bool IsUserSigned()
        {
            var user = Firebase.Auth.FirebaseAuth.GetInstance(MainActivity.App).CurrentUser;
            var signedIn = user != null;
            return signedIn;
        }
        #endregion

        public string getAuthKey()
        {
            throw new NotImplementedException();
        }

        public async Task<bool> Logout()
        {
            try
            {
                Firebase.Auth.FirebaseAuth.GetInstance(MainActivity.App).SignOut();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #region SignIn Google
        public void SignInWithGoogle()
        {
            var googleIntent = new Intent(Forms.Context, typeof(Activities.GoogleLoginActivity));
            ((Activity)Forms.Context).StartActivityForResult(googleIntent, GOOGLE_REQ_AUTH);
        }

        public async Task<bool> SignInWithGoogle(string token)
        {
            try
            {
                AuthCredential credential = GoogleAuthProvider.GetCredential(token, null);
                await Firebase.Auth.FirebaseAuth.GetInstance(MainActivity.App).SignInWithCredentialAsync(credential);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public void SignInWithFacebook()
        {
            throw new NotImplementedException();
        }
        public Task<bool> SignInWithFacebook(string token)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
