﻿using System;
using COKIAPP.Services;

namespace COKIAPP.Droid.Services
{
    public class FirebaseUserWrapper:IFirebaseUser
    {
		public Firebase.Auth.FirebaseUser User { get; private set; }

        public FirebaseUserWrapper(Firebase.Auth.FirebaseUser user)
        {
            this.User = user;
        }

        public string Uid => User.Uid;

        public string DisplayName => User.DisplayName;

        public bool IsAnonymous => User.IsAnonymous;

        public string Email => User.Email;

        public bool IsEmailVerified => User.IsEmailVerified;

        public string PhoneNumber => User.PhoneNumber;

        public string PhotoUrl => User.PhotoUrl.ToString();
    }
}
