﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Firebase;
using Android.Content;
using COKIAPP.Droid.Services;
using Android.Gms.Auth.Api.SignIn;

namespace COKIAPP.Droid
{
    [Activity(Label = "COKIAPP", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static FirebaseApp App { get; private set; }

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            InitFirebaseAuth();         //Init Firebase

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
        }
        private void InitFirebaseAuth()
        {
            var options = new FirebaseOptions.Builder()
                                             .SetApplicationId("1:701581888733:android:dfabdb79fc496541")
                                             .SetApiKey("AIzaSyDbQq0JP75JKPOaL7bxN9kglPy5vorSvl4")
                                             .Build();
            if (App == null)
                App = FirebaseApp.InitializeApp(this, options, "coki-app");
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == FirebaseAuthService.GOOGLE_REQ_AUTH && resultCode == Result.Ok)
            {
                GoogleSignInAccount sg = (GoogleSignInAccount)data.GetParcelableExtra("result");
                Xamarin.Forms.MessagingCenter.Send(FirebaseAuthService.KEY_AUTH_GOOGLE, FirebaseAuthService.KEY_AUTH_GOOGLE, sg.IdToken);
            }
            else
            {
                Toast.MakeText(this, "No pudimos conectarnos con Google", ToastLength.Long).Show();
            }
        }
    }
}

