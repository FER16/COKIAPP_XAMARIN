﻿using System;
using System.Collections.Generic;
using COKIAPP.Services;
using Xamarin.Forms;

namespace COKIAPP.Pages
{
    public partial class LoginPage : ContentPage
    {
        private IFirebaseAuthService _firebaseService;

        public LoginPage()
        {
            InitializeComponent();
        }
        void Registrarme_Clicked(object sender, System.EventArgs e)
        {
            Application.Current.MainPage.DisplayAlert("Coki", "FORMULARIO DE REGISTRO", "ACEPTAR");
        }

        void LogInFacebook_Clicked(object sender, System.EventArgs e)
        {
            throw new NotImplementedException();
        }

        void LogInGoogle_Clicked(object sender, System.EventArgs e)
        {
            App.FirebaseService.SignInWithGoogle();
        }

        void RecuperarCuenta_Clicked(object sender, System.EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
