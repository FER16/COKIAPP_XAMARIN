﻿using System;
using System.Threading.Tasks;

namespace COKIAPP.Services
{
    public interface IFirebaseAuthService
    {
        string GoogleAuthKey { get; }

        string FacebookAuthKey { get; }

        bool IsUserSigned();

        IFirebaseUser GetUser();

        void SignInWithGoogle();
        Task<bool> SignInWithGoogle(string token);

        void SignInWithFacebook();

        Task<bool> SignInWithFacebook(string token);

        Task<bool> Logout();  
    }
}
