﻿using System;
namespace COKIAPP.Services
{
    public interface IFirebaseUser
    {
        string Uid { get; }

        string DisplayName { get; }

        bool IsAnonymous { get; }

        string Email { get; }

        bool IsEmailVerified { get; }

        string PhoneNumber { get; }

        string PhotoUrl { get; }
    }
}
