using System;
using System.Threading.Tasks;
using Android.Widget;
using COKIAPP.Pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace COKIAPP
{
    public partial class App : Application
    {
        public static Services.IFirebaseAuthService FirebaseService { get; private set; }

        public App()
        {
            InitializeComponent();
            FirebaseService = DependencyService.Get<Services.IFirebaseAuthService>();

            MessagingCenter.Subscribe<String, String>(this, FirebaseService.GoogleAuthKey, (sender, args) =>
            {
               LoginGoogle(args);

            });

            MessagingCenter.Subscribe<String, String>(this, FirebaseService.FacebookAuthKey, (sender, args) =>
            {
                LoginFacebook(args);

            });

            if (FirebaseService.IsUserSigned())
                MainPage = CreateAppMainPage();//Cambiar nombre al metodo
            else
                MainPage = CreateLoginPage();
        }

        private async Task LoginGoogle(String token)
        {
            if (await FirebaseService.SignInWithGoogle(token))
            {
                MainPage = CreateAppMainPage();
            }
            else
            {
                //Toast.MakeText(typeof(App), "No pudimos conectarnos con Google", ToastLength.Long).Show();
            }
        }

        private async Task LoginFacebook(String token)
        {
            if (await FirebaseService.SignInWithFacebook(token))
            {
                MainPage = CreateAppMainPage();
            }else{
                //Toast.MakeText(typeof(App), "No pudimos conectarnos con Facebook", ToastLength.Long).Show();
            }
        }


        public static Page CreateAppMainPage()
        {
            return new NavigationPage(new Pages.RegisterPage());
        }

        public static Page CreateLoginPage()
        {
            return new Pages.LoginPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
